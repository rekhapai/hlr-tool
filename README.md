# HLR Tool

Install CIL

1. Download and install CIL from https://github.com/cil-project/cil

2. Unzip the files findStr.zip, firstStmt.zip, functionNames.zip, and locksetanalysis.zip (provided in the repo) to the directory /INSTALL_DIR/cil-develop/src/ext

3. run 'make' from the cil-develop directory

4. run 'make install' from the cil-develop directory




Detection of high level races in TIRTOS

1. Download and install the TI-RTOS v2.21.01.08 06 Feb 2018 for Linux from http://downloads.ti.com/dsps/dsps_public_sw/sdo_sb/targetcontent/tirtos/index.html

2. Add the files (provided in the repo) Swi_criticalAccesses.c, Task_criticalAccesses.c, LockSemantics.txt, toRemove.txt, variablesHLDR.txt, and hlr.sh to the directory /INSTALL_DIR/tirtos_cc13xx_cc26xx_2_21_01_08/products/bios_6_46_01_38/packages/ti/sysbios/knl

3. Give exec permission for hlr.sh

4. run './hlr' from the /ti/sysbios/knl directory

5. The list of races is generated in hldr_race.txt




Detection of high level races in FreeRTOS

1. Download and install the FreeRTOS v10.0.0 from https://sourceforge.net/projects/freertos/files/FreeRTOS/

2. Add the files (provided in the repo) queue_criticalAccesses.c, tasks_criticalAccesses.c, LockSemantics.txt, toRemove.txt, variablesHLDR.txt, and hlr_f.sh to the directory /INSTALL_DIR/trial/FreeRTOSv10.0.0/FreeRTOS/Source

3. Give exec permission for hlr_f.sh

4. run './hlr_f' from the /INSTALL_DIR/trial/FreeRTOSv10.0.0/FreeRTOS/Source directory

5. The list of races is generated in hldr_race.txt


